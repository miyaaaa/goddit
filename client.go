package goddit

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const baseAuthURL = "https://oauth.reddit.com"

// Client can be used to interact with reddit's API.
type Client struct {
	OAuthToken
	userAgent string
}

// New creates and returns a new client.
func New(token OAuthToken, userAgent string) *Client {
	return &Client{token, userAgent}
}

// GetBearer returns the formatted bearer token.
func (c *Client) GetBearer() string {
	return fmt.Sprintf("bearer %s", c.AccessToken)
}

func (c *Client) doGetRequest(url string, result interface{}) error {
	httpClient := http.Client{}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return fmt.Errorf("failed to create request: %v", err)
	}

	req.Header.Set("User-Agent", c.userAgent)
	req.Header.Set("Authorization", c.GetBearer())

	res, err := httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send request: %v", err)
	}
	defer res.Body.Close()

	if s := res.StatusCode; s != 200 {
		return fmt.Errorf("received status code: %d, expected 200", s)
	}

	if err := json.NewDecoder(res.Body).Decode(result); err != nil {
		return fmt.Errorf("failed to decode json: %v", err)
	}

	return nil
}
