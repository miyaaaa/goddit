package goddit

import "fmt"

// Comment represents a comment.
type Comment struct {
	SubredditID     string  `json:"subreddit_id"`
	ApprovedAtUTC   float64 `json:"approved_at_utc"`
	AuthorIsBlocked bool    `json:"author_is_blocked"`
	//Awarders [],
	//BannedBy
	TotalAwardsReceived int         `json:"total_awards_received"`
	Subreddit           string      `json:"subreddit"`
	Saved               bool        `json:"saved"`
	ID                  string      `json:"id"`
	BannedAtUTC         float64     `json:"banned_at_utc"`
	Gilded              int         `json:"gilded"`
	Archived            bool        `json:"archived"`
	Author              string      `json:"author"`
	SendReplies         bool        `json:"send_replies"`
	ParentID            string      `json:"parent_id"`
	Replies             interface{} `json:"replies"` // TODO: List<Thing>
	Score               int         `json:"score"`
	AuthorFullname      string      `json:"author_fullname"`
	Body                string      `json:"body"`
	BodyHTML            string      `json:"body_html"`
	Edited              bool        `json:"edited"`
	//"all_awardings": [],
	Collapsed bool   `json:"collapsed"`
	Name      string `json:"name"`
	Stickied  bool   `json:"stickied"`
	//"gildings": {},
	Permalink             string `json:"permalink"`
	Locked                bool   `json:"locked"`
	LinkID                string `json:"link_id"`
	SubredditNamePrefixed string `json:"subreddit_name_prefixed"`
	Controversiality      int    `json:"controversiality"`
	Depth                 int    `json:"depth"`
	//"mod_reports": [],
	//"num_reports": null,
	ApprovedBy          string `json:"approved_by"`
	AuthorFlairCSSClass string `json:"author_flair_css_class"`
	AuthorFlairText     string `json:"author_flair_text"`
	BannedBy            string `json:"banned_by"`
	LinkAuthor          string `json:"link_author"`
	LinkTitle           string `json:"link_title"`
	LinkURL             string `json:"link_url"`
	NumReports          int    `json:"num_reports"`
	ScoreHidden         bool   `json:"score_hidden"`
	Distinguished       string `json:"distinguished"`

	// Votable
	Ups   int         `json:"ups"`
	Downs int         `json:"downs"`
	Likes interface{} `json:"likes"`

	// Created
	Created    int `json:"created"`
	CreatedUTC int `json:"created_utc"`
}

// GetCommentsOfLink returns the comments belonging to the link identified by
// `fullname`.
func (c *Client) GetCommentsOfLink(linkID string) (*[]Comment, error) {
	url := fmt.Sprintf("%s/comments/%s", baseAuthURL, linkID)

	var comments []Comment
	err := c.doGetRequest(url, &comments)
	if err != nil {
		return nil, err
	}

	return &comments, nil
}
