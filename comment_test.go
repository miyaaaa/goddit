package goddit_test

import (
	"testing"

	"gitlab.com/miyaaaa/goddit"
)

func TestGetCommentsOfLink(t *testing.T) {
	a := goddit.GetAuthenticator("test_client_id", "test_user_agent")
	c := a.GetClient(token)

	_, err := c.GetCommentsOfLink("qtlmpn")
	if err != nil {
		t.Errorf("expected error to be nil: %v\n", err)
	}
}
