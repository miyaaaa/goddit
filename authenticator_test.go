package goddit_test

import (
	"testing"

	"gitlab.com/miyaaaa/goddit"
)

func TestGetTokenCodeFlowPermanent(t *testing.T) {
	a := goddit.GetAuthenticator("test_client_id", "test_user_agent")
	a.SetFlow(goddit.Code)
	a.SetDuration(goddit.Permanent)
	a.SetScope([]goddit.Scope{goddit.Read})
	a.SetCustomState("test_state")

	expectedURL := "https://www.reddit.com/api/v1/authorize?client_id=test_client_id&response_type=code&state=test_state&redirect_uri=http://127.0.0.1:8080/authorize_callback&duration=permanent&scope=read"
	gotURL := a.GetURL()

	if gotURL != expectedURL {
		t.Error("url doesn't match")
	}
}

func TestGetTokenImlicitFlowTemporary(t *testing.T) {
	a := goddit.GetAuthenticator("test_client_id", "test_user_agent")
	a.SetFlow(goddit.Token)
	a.SetDuration(goddit.Temporary)
	a.SetScope([]goddit.Scope{goddit.Read, goddit.Edit})
	a.SetCustomState("test_state")

	expectedURL := "https://www.reddit.com/api/v1/authorize?client_id=test_client_id&response_type=token&state=test_state&redirect_uri=http://127.0.0.1:8080/authorize_callback&duration=temporary&scope=read+edit"
	gotURL := a.GetURL()

	if gotURL != expectedURL {
		t.Error("url doesn't match")
	}
}

func TestGetAppOnlyTokenInstalled(t *testing.T) {
	// Don't worry, this token is not very confidential.
	a := goddit.GetAuthenticator("cd0Envm8gKyg0ZWtE6SsnA", "test_user_agent")
	token, err := a.RequestAppOnlyToken("DO_NOT_TRACK_THIS_DEVICE", "qHXz6AXU57ZI16Kb2NT_NHvZhR51-A", goddit.InstalledClient)

	if err != nil {
		t.Errorf("error was not nil: %v\n", err)
	}

	if token.AccessToken == "" {
		t.Error("received empty token")
	}
}

func TestGetAppOnlyTokenClient(t *testing.T) {
	// Don't worry, this token is not very confidential.
	a := goddit.GetAuthenticator("cd0Envm8gKyg0ZWtE6SsnA", "test_user_agent")
	token, err := a.RequestAppOnlyToken("DO_NOT_TRACK_THIS_DEVICE", "qHXz6AXU57ZI16Kb2NT_NHvZhR51-A", goddit.ClientCredentials)

	if err != nil {
		t.Errorf("error was not nil: %v\n", err)
	}

	if token.AccessToken == "" {
		t.Error("received empty token")
	}
}
