package goddit

import (
	"fmt"
)

// Account represents a reddit account.
type Account struct {
	IsEmployee           bool      `json:"is_employee"`
	IsFriend             bool      `json:"is_friend"`
	SeenLayoutSwitch     bool      `json:"seen_layout_switch"`
	HasVisitedNewProfile bool      `json:"has_visited_new_profile"`
	PrefNoProfanity      bool      `json:"pref_no_profanity"`
	HasExternalAccount   bool      `json:"has_external_account"`
	PrefGeopopular       string    `json:"pref_geopopular"`
	SeenRedesignModal    bool      `json:"seen_redesign_modal"`
	PrefShowTrending     bool      `json:"pref_show_trending"`
	Subreddit            Subreddit `json:"subreddit"`
	PrefShowPresence     bool      `json:"pref_show_presence"`
	SnoovatarIMG         string    `json:"snoovatar_img"`
	SnoovatarSize        []int     `json:"snoovatar_size"`
	GoldExpiration       float64   `json:"gold_expiration"`
	HasGoldSubscription  bool      `json:"has_gold_subscription"`
	IsSponsor            bool      `json:"is_sponsor"`
	NumFriends           int       `json:"num_friends"`
	Features             struct {
		ModServiceMuteWrites      bool `json:"mod_service_mute_writes"`
		PromotedTrendBlanks       bool `json:"promoted_trend_blanks"`
		ShowAmpLink               bool `json:"show_amp_link"`
		IsEmailPermissionRequired bool `json:"is_email_permission_required"`
		ModAwards                 bool `json:"mod_awards"`
		ExpensiveCoinsPackage     bool `json:"expensive_coins_package"`
		AwardsOnStreams           bool `json:"awards_on_streams"`
		ChatSubreddit             bool `json:"chat_subreddit"`
		CookieConsentBanner       bool `json:"cookie_consent_banner"`
		ModlogCopyrightRemoval    bool `json:"modlog_copyright_removal"`
		ShowNPSSurvey             bool `json:"show_nps_survey"`
		DoNotTrack                bool `json:"do_not_track"`
		ModServiceMuteReads       bool `json:"mod_service_mute_reads"`
		ChatUserSettings          bool `json:"chat_user_settings"`
		UsePrefAccountDeployment  bool `json:"use_pref_account_deployment"`
		PremiumSubscriptionsTable bool `json:"premium_subscriptions_table"`
		NoreffererToNoopener      bool `json:"noreferrer_to_noopener"`
		ChatGroupRollout          bool `json:"chat_group_rollout"`
		ResizedStyleImages        bool `json:"resized_styles_images"`
		SpezModal                 bool `json:"spez_modal"`
	} `json:"features"`
	CanEditName             bool    `json:"can_edit_name"`
	Verified                bool    `json:"verified"`
	NewModmailExists        bool    `json:"new_modmail_exists"`
	PrefAutoplay            bool    `json:"pref_autoplay"`
	Coins                   int     `json:"coins"`
	HasSubscribedToPremium  bool    `json:"has_subscribed_to_premium"`
	ID                      string  `json:"id"`
	OAuthClientID           string  `json:"oauth_client_id"`
	CanCreateSubreddit      bool    `json:"can_create_subreddit"`
	Over18                  bool    `json:"over_18"`
	IsGold                  bool    `json:"is_gold"`
	IsMod                   bool    `json:"is_mod"`
	AwarderKarma            int     `json:"awarder_karma"`
	SuspensionExpirationUTC int     `json:"suspension_expiration_utc"`
	HasVerifiedEMail        bool    `json:"has_verified_email"`
	IsSuspended             bool    `json:"is_suspended"`
	PrefVideoAutoplay       bool    `json:"pref_video_autoplay"`
	IconIMG                 string  `json:"icon_img"`
	HasModMail              bool    `json:"has_mod_mail"`
	PrefNightmode           bool    `json:"pref_nightmode"`
	AwardeeKarma            int     `json:"awardee_karma"`
	HideFromRobots          bool    `json:"hide_from_robots"`
	PasswordSet             bool    `json:"password_set"`
	LinkKarma               int     `json:"link_karma"`
	ForcePasswordReset      bool    `json:"force_password_reset"`
	TotalKarma              int     `json:"total_karma"`
	PrefTopKarmaSubreddits  bool    `json:"pref_top_karma_subreddits"`
	HasMail                 bool    `json:"has_mail"`
	PrefShowSnoovatar       bool    `json:"pref_show_snoovatar"`
	Name                    string  `json:"name"`
	Created                 float64 `json:"created"`
	GoldCreddits            int     `json:"gold_creddits"`
	CreatedUTC              float64 `json:"created_utc"`
	PrefShowTwitter         bool    `json:"pref_show_twitter"`
	InBeta                  bool    `json:"in_beta"`
	CommentKarma            int     `json:"comment_karma"`
	HasSubscribed           bool    `json:"has_subscribed"`
}

// GetMe retrieves the logged in account.
func (c *Client) GetMe() (*Account, error) {
	url := fmt.Sprintf("%s/api/v1/me", baseAuthURL)

	var acc Account
	err := c.doGetRequest(url, &acc)
	if err != nil {
		return nil, err
	}

	return &acc, nil
}

// GetAccount retrieves a user's account.
func (c *Client) GetAccount(name string) (*Account, error) {
	url := fmt.Sprintf("%s/user/%s/about", baseAuthURL, name)

	var acc Account
	err := c.doGetRequest(url, &acc)
	if err != nil {
		return nil, err
	}

	return &acc, nil
}
