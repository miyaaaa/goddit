package goddit

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	redirectURI = "http://127.0.0.1:8080/authorize_callback"
	authURI     = "https://www.reddit.com/api/v1/authorize?client_id=%s&response_type=%s&state=%s&redirect_uri=%s&duration=%s&scope=%s"
	tokenURI    = "https://www.reddit.com/api/v1/access_token"
	revokeURI   = "https://www.reddit.com/api/v1/revoke_token"
)

// GrantType determines whether the app acts on behalf of a user, or on itself.
type GrantType int

const (
	// Authorization is used for apps that act on behalf of a user.
	Authorization GrantType = iota

	// ClientCredentials is used for confidential web apps or scripts, that are not acting on behalf of a user (application-only OAuth).
	ClientCredentials

	// InstalledClient is used for installed apps that are not acting on behalf of a user (application-only OAuth).
	InstalledClient
)

// Flow determines the type of the authorization.
// "Web" and "Script" apps must use type `Code`, "Installed" apps
// may use type `Token`.
// In reddit's API this is called the `response_type`.
type Flow string

const (
	// Code is used for "code" flow authorization. Use this for applications of type "web" and "script".
	Code Flow = "code"

	// Token is used for "implicit" grant flow. Only use this for "installed" apps.
	Token Flow = "token"
)

// Duration indicates whether or not a permanent token is needed. All bearer
// tokens expire after 1 hour. If a permanent token is requested, a
// `refresh_token` can be used to acquire a new bearer token after the current
// token expires.
type Duration string

const (
	// Temporary requests a temporary token (one-time use only).
	Temporary Duration = "temporary"

	// Permanent requests a permanent token, an additional `refresh_token` may be used to request a new token after the current token expires.
	Permanent Duration = "permanent"
)

// Scope defines the different areas of the API that can be accessed.
type Scope string

// Scope type enumeration.
const (
	Identity        Scope = "identity"
	Edit            Scope = "edit"
	Flair           Scope = "flair"
	History         Scope = "history"
	Modconfig       Scope = "modconfig"
	Modflair        Scope = "modflair"
	Modlog          Scope = "modlog"
	Modposts        Scope = "modposts"
	Modwiki         Scope = "modwiki"
	Mysubreddits    Scope = "mysubreddits"
	Privatemessages Scope = "privatemessages"
	Read            Scope = "read"
	Report          Scope = "report"
	Save            Scope = "save"
	Submit          Scope = "submit"
	Subscribe       Scope = "subscribe"
	Vote            Scope = "vote"
	Wikiedit        Scope = "wikiedit"
	Wikiread        Scope = "wikiread"
)

// OAuthToken represents an OAuth response containing the access token.
type OAuthToken struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"` // Expires after 3600s (1h)
	Scope        string `json:"scope"`
	RefreshToken string `json:"refresh_token"`
	State        string `json:"state"`
}

// Authenticator provides methods to authenticate, renew and revoke tokens.
type Authenticator struct {
	clientID    string
	userAgent   string
	redirectURI string
	flow        Flow
	duration    Duration
	scopes      []Scope
	state       string

	// TODO: This is stupid.
	listener   *http.Server
	oAuthToken OAuthToken
	authError  error
}

// GetAuthenticator creates and returns a new Authenticator.
func GetAuthenticator(clientID string, userAgent string) *Authenticator {
	rand.Seed(time.Now().UnixNano())
	state := randState(10)

	auth := &Authenticator{
		clientID:    clientID,
		userAgent:   userAgent,
		state:       state,
		redirectURI: redirectURI,
	}

	return auth
}

// SetRedirectURI sets a custom redirect URI.
// The default redirect URI is "http://127.0.0.1:8080/authorize_callback"
func (a *Authenticator) SetRedirectURI(uri string) {
	a.redirectURI = uri
}

// SetScope sets the scope of the authorization.
func (a *Authenticator) SetScope(scopes []Scope) {
	a.scopes = scopes
}

// SetFlow sets the authentication flow.
func (a *Authenticator) SetFlow(flow Flow) {
	a.flow = flow
}

// SetDuration sets the duration of the authorization.
func (a *Authenticator) SetDuration(duration Duration) {
	a.duration = duration
}

// SetCustomState sets a custom state.
func (a *Authenticator) SetCustomState(state string) {
	a.state = state
}

// GetState returns the state.
func (a *Authenticator) GetState() string {
	return a.state
}

// GetURL returns the URL a user needs to visit in order to authorize and
// retrieve a token.
func (a *Authenticator) GetURL() string {
	return fmt.Sprintf(authURI, a.clientID, a.flow, a.state, a.redirectURI,
		a.duration, url.QueryEscape(scopeString(a.scopes)))
}

// Abort aborts the currently running token request.
func (a *Authenticator) Abort() {
	stopListener(a.listener)
}

// RequestToken requests a new token.
func (a *Authenticator) RequestToken() (OAuthToken, error) {
	a.createListener()
	a.listener.ListenAndServe() // wait for response

	// TODO: I don't like it at all, how we stop the listener and get the
	// result.
	if a.authError != nil {
		return OAuthToken{}, a.authError
	}

	return a.oAuthToken, nil
}

// Renew requests a new token after the old token expired.
func (a *Authenticator) Renew(refreshToken string) (OAuthToken, error) {
	httpClient := http.Client{}

	data := url.Values{}
	data.Add("grant_type", "refresh_token")
	data.Add("refresh_token", refreshToken)

	req, err := http.NewRequest(http.MethodPost, tokenURI, strings.NewReader(data.Encode()))
	if err != nil {
		return OAuthToken{}, fmt.Errorf("failed to create request: %v", err)
	}

	req.Header.Set("User-Agent", a.userAgent)
	req.Header.Set("Authorization", "Basic "+basicAuth(a.clientID, ""))

	res, err := httpClient.Do(req)

	if err != nil {
		return OAuthToken{}, fmt.Errorf("failed to send request: %v", err)
	}

	defer res.Body.Close()

	if s := res.StatusCode; s != 200 {
		return OAuthToken{}, fmt.Errorf("received status code: %d, expected 200", s)
	}

	if err := json.NewDecoder(res.Body).Decode(&a.oAuthToken); err != nil {
		return OAuthToken{}, fmt.Errorf("failed to decode response: %v", err)
	}

	return a.oAuthToken, nil
}

// Revoke revokes an active token.
// If `accessTokenOnly` is set to `true`, only the access-token itself will be
// revoked, while leaving the refresh-token working.
func (a *Authenticator) Revoke(token OAuthToken, accessTokenOnly bool) error {
	httpClient := http.Client{}
	data := url.Values{}

	// NOTE: Revoking an access-token will leave the refresh-token working.
	// Revoking the refresh_token on the other hand will also revoke the
	// access token.
	if accessTokenOnly {
		data.Add("token", token.AccessToken)
		data.Add("token_type_hint", "access_token")
	} else {
		data.Add("token", token.RefreshToken)
		data.Add("token_type_hint", "refresh_token")
	}

	req, err := http.NewRequest(http.MethodPost, revokeURI, strings.NewReader(data.Encode()))
	if err != nil {
		return fmt.Errorf("failed to create request: %v", err)
	}

	req.Header.Set("User-Agent", a.userAgent)
	req.Header.Set("Authorization", "Basic "+basicAuth(a.clientID, ""))

	res, err := httpClient.Do(req)

	if err != nil {
		return fmt.Errorf("failed to send request: %v", err)
	}

	defer res.Body.Close()

	if s := res.StatusCode; s != 200 && s != 204 {
		return fmt.Errorf("received status code: %d, expected 200 or 204", s)
	}

	return nil
}

// RequestAppOnlyToken request an application-only OAuth-Token.
// Use this if your app doesn't act on behalf of a user.
// Set `deviceID` to "DO_NOT_TRACK_THIS_DEVICE" to instruct reddit to not
// track the device.
// The `clientSecret` is the secret from your [app](https://www.reddit.com/prefs/apps)
func (a *Authenticator) RequestAppOnlyToken(deviceID, clientSecret string, grantType GrantType) (OAuthToken, error) {
	httpClient := http.Client{}
	data := url.Values{}

	if grantType == ClientCredentials {
		data.Add("grant_type", "client_credentials")
	} else if grantType == InstalledClient {
		data.Add("grant_type", "https://oauth.reddit.com/grants/installed_client")
		data.Add("device_id", deviceID)
	}

	req, err := http.NewRequest(http.MethodPost, tokenURI, strings.NewReader(data.Encode()))
	if err != nil {
		return OAuthToken{}, fmt.Errorf("failed to create request: %v", err)
	}

	req.Header.Set("User-Agent", a.userAgent)
	req.Header.Set("Authorization", "Basic "+basicAuth(a.clientID, clientSecret))

	res, err := httpClient.Do(req)

	if err != nil {
		return OAuthToken{}, fmt.Errorf("failed to send request: %v", err)
	}

	defer res.Body.Close()

	if s := res.StatusCode; s != 200 {
		return OAuthToken{}, fmt.Errorf("received status code: %d, expected 200", s)
	}

	if err := json.NewDecoder(res.Body).Decode(&a.oAuthToken); err != nil {
		return OAuthToken{}, fmt.Errorf("failed to decode response: %v", err)
	}

	return a.oAuthToken, nil
}

// GetClient creates and returns a new client using the supplied auth token.
func (a *Authenticator) GetClient(token OAuthToken) *Client {
	return New(token, a.userAgent)
}

// --- Internal functions --- //

func scopeString(scopes []Scope) string {
	if scopes == nil {
		return ""
	}

	str := ""
	for _, s := range scopes {
		str += string(s) + " "
	}
	str = str[:len(str)-1]
	return str
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randState(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func (a *Authenticator) createListener() {
	servMux := http.NewServeMux()
	authServer := http.Server{Addr: ":8080", Handler: servMux}
	httpClient := http.Client{}

	// Setup server to listen for reddit's authorization response.
	servMux.HandleFunc("/authorize_callback", func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			a.authError = fmt.Errorf("parsing form failed: %v", err)
			a.Abort()
		}

		answerError := r.FormValue("error")
		answerCode := r.FormValue("code")
		answerState := r.FormValue("state")

		if answerError != "" {
			a.authError = fmt.Errorf("got error: %s", answerError)
			a.Abort()
		}

		if answerState != a.state {
			a.authError = fmt.Errorf("received invalid state")
			a.Abort()
		}

		// Request access token.
		data := url.Values{}
		data.Set("grant_type", "authorization_code")
		data.Set("code", answerCode)
		data.Set("redirect_uri", a.redirectURI)

		req, err := http.NewRequest(http.MethodPost, tokenURI, strings.NewReader(data.Encode()))
		if err != nil {
			a.authError = fmt.Errorf("failed to create request: %v", err)
			a.Abort()
		}

		req.Header.Set("User-Agent", a.userAgent)
		req.Header.Set("Authorization", "Basic "+basicAuth(a.clientID, ""))

		// Send the http request to retrieve the access token.
		res, err := httpClient.Do(req)
		if err != nil {
			a.authError = fmt.Errorf("failed to send request: %v", err)
			a.Abort()
		}
		defer res.Body.Close()

		if s := res.StatusCode; s != 200 {
			a.authError = fmt.Errorf("received status code: %d, expected 200", s)
		}

		if err := json.NewDecoder(res.Body).Decode(&a.oAuthToken); err != nil {
			a.authError = fmt.Errorf("failed to decode response: %v", err)
			a.Abort()
		}

		stopListener(&authServer)
	})

	a.listener = &authServer
}

func stopListener(listener *http.Server) bool {
	if listener == nil {
		return false
	}

	listener.Shutdown(context.Background())
	return true
}
