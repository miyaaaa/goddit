package goddit_test

import (
	"testing"

	"gitlab.com/miyaaaa/goddit"
)

func TestGetMe(t *testing.T) {
	a := goddit.GetAuthenticator("test_client_id", "test_user_agent")
	c := a.GetClient(token)

	_, err := c.GetMe()
	if err != nil {
		t.Errorf("expected error to be nil: %v\n", err)
	}
}

func TestGetAccount(t *testing.T) {
	a := goddit.GetAuthenticator("test_client_id", "test_user_agent")
	c := a.GetClient(token)

	_, err := c.GetAccount("SCIP-TEST")
	if err != nil {
		t.Errorf("expected error to be nil: %v\n", err)
	}
}
