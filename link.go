package goddit

import "fmt"

// Link represents a post.
type Link struct {
	ApprovedAtUTC         float64     `json:"approved_at_utc"`
	Subreddit             string      `json:"subreddit"`
	SelfText              string      `json:"selftext"`
	AuthorFullname        string      `json:"author_fullname"`
	Saved                 bool        `json:"saved"`
	ModReasonTitle        string      `json:"mod_reason_title"`
	Gilded                int         `json:"gilded"`
	Clicked               bool        `json:"clicked"`
	Title                 string      `json:"title"`
	SubredditNamePrefixed string      `json:"subreddit_name_prefixed"`
	Hidden                bool        `json:"hidden"`
	Pwls                  int         `json:"pwls"`
	LinkFlairCSSClass     string      `json:"link_flair_css_class"`
	HideScore             bool        `json:"hide_score"`
	Name                  string      `json:"name"`
	Quarantine            bool        `json:"quarantine"`
	UpvoteRatio           float32     `json:"upvote_ratio"`
	TotalAvardsReceived   int         `json:"total_awards_received"`
	MediaEmbed            interface{} `json:"media_embed"` // TODO: MediaEmbed
	IsOriginalContent     bool        `json:"is_original_content"`
	//UserReports []
	IsMeta        bool   `json:"is_meta"`
	LinkFlairText string `json:"link_flair_text"`
	Score         int    `json:"score"`
	//ApprovedBy
	Thumbnail           string   `json:"thumbnail"`
	Edited              int      `json:"edited"`
	AuthorFlairCSSClass string   `json:"author_flair_css_class"`
	AuthorFlairRichText []string `json:"author_flair_rich_text"`
	IsSelf              bool     `json:"is_self"`
	//ModNote
	WLS int `json:"wls"`
	//BannedBy
	Domain          string      `json:"domain"`
	SelfTextHTML    string      `json:"selftext_html"`
	BannedAtUTC     float64     `json:"banned_at_utc"`
	ViewCount       int         `json:"view_count"`
	Archived        bool        `json:"archived"`
	IsCrosspostable bool        `json:"is_crosspostable"`
	Pinned          bool        `json:"pinned"`
	Over18          bool        `json:"over18"`
	AllAwardings    interface{} `json:"awardings"`
	Author          string      `json:"author"`
	Locked          bool        `json:"locked"`
	Media           interface{} `json:"media"` // TODO: Media
	NumComments     int         `json:"num_comments"`
	PermaLink       string      `json:"permalink"`
	SubredditID     string      `json:"subreddit_id"`
	Stickied        bool        `json:"stickied"`
	Distinguished   string      `json:"distinguished"`
	URL             string      `json:"url"`

	// Votable
	Ups   int         `json:"ups"`
	Downs int         `json:"downs"`
	Likes interface{} `json:"likes"`

	// Created
	Created    int `json:"created"`
	CreatedUTC int `json:"created_utc"`
}

// GetLink retrieves the link identified by `fullname`.
func (c *Client) GetLink(fullname string) (*Link, error) {
	url := fmt.Sprintf("%s/by_id/%s", baseAuthURL, fullname)

	var link Link
	err := c.doGetRequest(url, &link)
	if err != nil {
		return nil, err
	}

	return &link, nil
}
