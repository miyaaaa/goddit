package goddit

import (
	"fmt"
)

// Subreddit represents a subreddit.
type Subreddit struct {
	SubmitTextHTML      string `json:"submit_text_html"`
	RestrictPosting     bool   `json:"restrict_posting"`
	UserIsBanned        bool   `json:"user_is_banned"`
	FreeFormReports     bool   `json:"free_form_reports"`
	WikiEnabled         bool   `json:"wiki_enabled"`
	UserIsMuted         bool   `json:"user_is_muted"`
	UserCanFlairInSR    bool   `json:"user_can_flair_in_sr"`
	DisplayName         string `json:"display_name"`
	HeaderIMG           string `json:"header_img"`
	Title               string `json:"title"`
	AllowGalleries      bool   `json:"allow_galleries"`
	IconSize            []int  `json:"icon_size"`
	ActiveUserCount     int    `json:"active_user_count"`
	IconIMG             string `json:"icon_img"`
	DisplayNamePrefixed string `json:"display_name_prefixed"`
	AccountsActive      int    `json:"accounts_active"`
	PublicTraffic       bool   `json:"public_traffic"`
	Subscribers         int    `json:"subscribers"`
	//TODO: UserFlairRichtext []
	VideoStreamLinksCount          int         `json:"videostream_links_count"`
	Name                           string      `json:"name"`
	Quarantine                     bool        `json:"quarantine"`
	HideAds                        bool        `json:"hide_ads"`
	PredictionLeaderboardEntryType string      `json:"prediction_leaderboard_entry_type"`
	EmojisEnabled                  bool        `json:"emojis_enabled"`
	AdvertiserCategory             string      `json:"advertiser_category"`
	PublicDescription              string      `json:"public_description"`
	CommentScoreHideMins           int         `json:"comment_score_hide_mins"`
	AllowPredictions               bool        `json:"allow_predictions"`
	UserHasFavorited               bool        `json:"user_has_favorited"`
	UserFlairTemplateID            interface{} `json:"user_flair_template_id"`
	CommunityIcon                  string      `json:"community_icon"`
	BannerBackgroundIMG            string      `json:"banner_background_image"`
	OriginalContentTagEnabled      bool        `json:"original_content_tag_enabled"`
	CommunityReviewed              bool        `json:"community_reviewed"`
	SubmitText                     string      `json:"submit_text"`
	DescriptionHTML                string      `json:"description_html"`
	SpoilersEnabled                bool        `json:"spoilers_enabled"`
	HeaderTitle                    string      `json:"header_title"`
	HeaderSize                     []int       `json:"header_size"` // width and height
	AllOriginalContent             bool        `json:"all_original_content"`
	CanAssignUserFlair             bool        `json:"can_assign_user_flair"`
	Created                        float64     `json:"created"`
	WLS                            int         `json:"wls"`
	ShowMediaPreview               bool        `json:"show_media_preview"`
	SubredditType                  string      `json:"subreddit_type"`
	UserIsSubscriber               bool        `json:"user_is_subscriber"`
	DisableContributorRequests     bool        `json:"disable_contributor_requests"`
	AllowVideoGifs                 bool        `json:"allow_videogifs"`
	ShouldArchivePosts             bool        `json:"should_archive_posts"`
	UserFlairType                  string      `json:"user_flair_type"`
	AllowPolls                     bool        `json:"allow_polls"`
	CollapseDeletedComments        bool        `json:"collapse_deleted_comments"`
	PublicDescriptionHTML          string      `json:"public_description_html"`
	AllowVideos                    bool        `json:"allow_videos"`
	IsCrosspostableSubreddit       bool        `json:"is_crosspostable_subreddit"`
	NotificationLevel              interface{} `json:"notification_level"`
	SubmitTextLabel                string      `json:"submit_text_label"`
	AllowDiscover                  bool        `json:"allow_discovery"`
	AcceptFollowers                bool        `json:"accept_followers"`
	BannerIMG                      string      `json:"banner_img"`
	ID                             string      `json:"id"`
	UserIsModerator                bool        `json:"user_is_moderator"`
	Description                    string      `json:"description"`
	SubmitLinkLabel                string      `json:"submit_link_label"`
	Lang                           string      `json:"lang"`
	URL                            string      `json:"url"`
	CreatedUTC                     float64     `json:"created_utc"`
	UserIsContributor              bool        `json:"user_is_contributor"`
	Over18                         bool        `json:"over18"`
	SubmissionType                 string      `json:"submission_type"`
}

// GetSubreddit retrieves a subreddit.
func (c *Client) GetSubreddit(name string) (*Subreddit, error) {
	url := fmt.Sprintf("%s/r/%s/about", baseAuthURL, name)

	var sub Subreddit
	err := c.doGetRequest(url, &sub)
	if err != nil {
		return nil, err
	}

	return &sub, nil
}
