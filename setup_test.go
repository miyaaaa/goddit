package goddit_test

import (
	"encoding/json"
	"log"
	"os"
	"testing"

	"gitlab.com/miyaaaa/goddit"
)

var token goddit.OAuthToken

func TestMain(m *testing.M) {
	cnt, err := os.ReadFile("token")
	if err != nil {
		log.Fatalf("could not read file, skipping tests: %v\n", err)
	}

	err = json.Unmarshal(cnt, &token)
	if err != nil {
		log.Fatalf("failed deserializing file, skipping tests: %v\n", err)
	}

	os.Exit(m.Run())
}
