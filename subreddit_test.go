package goddit_test

import (
	"testing"

	"gitlab.com/miyaaaa/goddit"
)

func TestGetSubreddit(t *testing.T) {
	a := goddit.GetAuthenticator("test_client_id", "test_user_agent")
	c := a.GetClient(token)

	_, err := c.GetSubreddit("Linux")

	if err != nil {
		t.Errorf("expected error to be nil: %v\n", err)
	}
}
